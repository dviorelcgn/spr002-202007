package bo.com.cognos.springbootmvc01.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import bo.com.cognos.springbootmvc01.entidades.Rol;
import bo.com.cognos.springbootmvc01.entidades.Usuario;

public interface RolRepository extends JpaRepository<Rol, Integer> {
	
}
