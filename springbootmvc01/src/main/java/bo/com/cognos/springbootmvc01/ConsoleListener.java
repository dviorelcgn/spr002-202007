package bo.com.cognos.springbootmvc01;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import bo.com.cognos.springbootmvc01.service.UsuarioService;

@Component
public class ConsoleListener implements CommandLineRunner {

	@Autowired
	UsuarioService usuarioService;
	
	@Override
	public void run(String... args) throws Exception {
		usuarioService.inicializar();
	}

}
