package bo.com.cognos.springbootmvc01;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

	@GetMapping("/login")
	public String irALogin() {
		return "login";
	}
		
}
