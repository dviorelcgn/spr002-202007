package bo.com.cognos.springbootmvc01;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import bo.com.cognos.springbootmvc01.entidades.Persona;

@Controller
public class DemoController {

//	//@GetMapping("/")
//	@RequestMapping(value = "/", method=RequestMethod.GET)
//	//@PostMapping("/")
//	//@RequestMapping(value = "/", method=RequestMethod.POST)
//	public String irAInicio(Model model) {
//    	model.addAttribute("mensaje", "Mensaje desde MVC con RequestMapping"); 
//        return "inicio";
//	}
	
	@RequestMapping(value = "/", method=RequestMethod.GET)
	public ModelAndView irAInicio() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("inicio");
		mav.addObject("mensaje", "Mensaje desde MVC con ModelANdView");
		Persona p = new Persona();
		p.setCi("1234");
		p.setNom_bre("Danny");
		p.setApellido("Viorel");
		mav.addObject("persona", p);
    	return mav;
	}

	
}
