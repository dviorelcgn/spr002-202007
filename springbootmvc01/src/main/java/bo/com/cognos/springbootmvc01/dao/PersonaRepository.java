package bo.com.cognos.springbootmvc01.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import bo.com.cognos.springbootmvc01.entidades.Persona;

public interface PersonaRepository extends JpaRepository<Persona, String>{

	
	
}