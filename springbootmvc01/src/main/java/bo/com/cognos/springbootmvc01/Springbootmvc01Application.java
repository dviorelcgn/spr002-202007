package bo.com.cognos.springbootmvc01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class Springbootmvc01Application extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(Springbootmvc01Application.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(Springbootmvc01Application.class);
	}


}
