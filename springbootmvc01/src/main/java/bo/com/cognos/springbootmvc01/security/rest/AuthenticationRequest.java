package bo.com.cognos.springbootmvc01.security.rest;

import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationRequest {

		   private String username;
		   private String password;
	
	
}
