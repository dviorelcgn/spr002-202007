package bo.com.cognos.springbootmvc01.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import bo.com.cognos.springbootmvc01.CustomRowMapper;
import bo.com.cognos.springbootmvc01.dao.ArticuloRepository;
import bo.com.cognos.springbootmvc01.dao.PersonaRepository;
import bo.com.cognos.springbootmvc01.entidades.Articulo;
import bo.com.cognos.springbootmvc01.entidades.Persona;
import bo.com.cognos.springbootmvc01.entidades.exception.ApplicationException;

@Service
@Transactional(readOnly = true)
public class ArticuloService {
	
	@Autowired
	ArticuloRepository repository;
	
	public List<Articulo> listar(){
		return repository.findAll();
	}

	public Articulo obtenerPorCodigo(String codigo){
		return repository.findByCodigo(codigo);
	}
	
	String RUTA = "d:/spr002/uploads/";
	
	public Articulo obtener(Integer id){
		Articulo articulo = repository.findById(id).orElse(null);
		if(articulo.getRutaArchivo() != null) {
			try {
				byte[] imagen = Files.readAllBytes(Paths.get(RUTA + articulo.getRutaArchivo()));
				articulo.setArchivo(imagen);
			} catch (IOException e) {
			}	
		}
		return articulo; 
	}
	
	@Transactional(rollbackFor = ApplicationException.class, noRollbackFor = RuntimeException.class,
			propagation = Propagation.REQUIRES_NEW)
	public void agregar(Articulo articulo) throws ApplicationException {
		if(articulo.getPrecio().compareTo(BigDecimal.ZERO) <= 0) {
			throw new ApplicationException("El precio debe ser positivo");
		}
		Articulo existente = repository.findByCodigo(articulo.getCodigo());
		if(existente != null) {
			throw new ApplicationException("Ya existe el articulo " + articulo.getDescripcion()
				+ " con el codigo " + articulo.getCodigo());
		}
		repository.save(articulo);
	}
	
	@Transactional(rollbackFor = ApplicationException.class)
	public void editar(Articulo articulo) throws ApplicationException {
		if(articulo.getPrecio().compareTo(BigDecimal.ZERO) <= 0) {
			throw new ApplicationException("El precio debe ser positivo");
		}
		Articulo existente = repository.findByCodigo(articulo.getCodigo());
		if(existente != null && !existente.getId().equals(articulo.getId())) {
			throw new ApplicationException("Ya existe el articulo " + articulo.getDescripcion()
				+ " con el codigo " + articulo.getCodigo());
		}
		repository.saveAndFlush(articulo);
	}
	
	@Transactional(rollbackFor = ApplicationException.class, propagation = Propagation.REQUIRED)
	public void eliminar(Integer idEliminar) {
		repository.deleteById(idEliminar);
	}
	
	
	@Autowired
	PersonaService personaService;
	
	
	
	
}
