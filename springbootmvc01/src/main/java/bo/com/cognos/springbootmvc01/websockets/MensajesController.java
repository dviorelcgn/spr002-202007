package bo.com.cognos.springbootmvc01.websockets;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

@Controller
public class MensajesController {

	@MessageMapping("/websockets")
	@SendTo("/topicos/mensajes")
	public ACliente saludar(AServidor mensaje){
		System.out.println("Mensaje: " + mensaje);
		return new ACliente("Hola " + HtmlUtils.htmlEscape(mensaje.getMensaje()) + "!");
	}

	
}
