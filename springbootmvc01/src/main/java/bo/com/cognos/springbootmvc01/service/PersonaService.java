package bo.com.cognos.springbootmvc01.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bo.com.cognos.springbootmvc01.CustomRowMapper;
import bo.com.cognos.springbootmvc01.dao.PersonaRepository;
import bo.com.cognos.springbootmvc01.entidades.Articulo;
import bo.com.cognos.springbootmvc01.entidades.Persona;
import bo.com.cognos.springbootmvc01.entidades.exception.ApplicationException;

@Service
@Transactional(readOnly = true)
public class PersonaService {
	
	@Autowired
	PersonaRepository repository;
	
//	@Autowired
//	JdbcTemplate jdbcTemplate;
	
//	private static PersonaService instancia = new PersonaService();
//	
//	private PersonaService() {
//		personas = new ArrayList<>();
//		personas.add(new Persona(UUID.randomUUID().toString(), "1111", "Nombre 1", "Apellido 1"));
//		personas.add(new Persona(UUID.randomUUID().toString(), "2222", "Nombre 2", "Apellido 2"));
//		personas.add(new Persona(UUID.randomUUID().toString(), "3333", "Nombre 3", "Apellido 3"));
//	}
//	
//	public static PersonaService getInstance() {
//		return instancia;
//	}
	
	//private List<Persona> personas;
	
	public List<Persona> listar(){
		//return personas;
//		String sql = "select * from persona";
		//return jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(Persona.class));
//		return jdbcTemplate.query(sql, new CustomRowMapper());
		return repository.findAll();
	}
	
	public Persona obtener(String id){
//		for(Persona persona: this.personas) {
//			if(persona.getId().equals(id)) {
//				return persona;
//			}
//		}		
//		return null;
		//return this.personas.stream().filter(persona -> persona.getId().equals(id)).findFirst().orElse(null);
//		String sql = "select * from persona where id = ?";
		//return jdbcTemplate.queryForObject(sql, new Object[] {id}, BeanPropertyRowMapper.newInstance(Persona.class));
//		return jdbcTemplate.queryForObject(sql, new Object[] {id}, new CustomRowMapper());
		return repository.getOne(id);
	}
	
	@Transactional
	public void agregar(Persona persona) {
		persona.setId(UUID.randomUUID().toString());
		//this.personas.add(persona);
		//String sql = "insert into persona (id, ci, nombre, apellido) values(?, ?, ?, ?) ";
		//jdbcTemplate.update(sql, persona.getId(), persona.getCi(), persona.getNom_bre(), persona.getApellido());
		repository.save(persona);
	}
	
	@Transactional
	public void editar(Persona personaEdicion) {
		/*Optional<Persona> p = this.personas.stream().filter(persona -> persona.getId().equals(personaEdicion.getId())).findFirst();
		if(p.isPresent()) {
			p.get().setApellido(personaEdicion.getApellido());
			p.get().setNombre(personaEdicion.getNombre());
		}*/
		//String sql = "update persona set ci = ?, nombre = ?, apellido = ? WHERE id = ?";
		//jdbcTemplate.update(sql, personaEdicion.getCi(), personaEdicion.getNom_bre(), personaEdicion.getApellido(), personaEdicion.getId());
		repository.saveAndFlush(personaEdicion);
	}
	
	@Transactional
	public void eliminar(String idEliminar) {
		/*Optional<Persona> p = this.personas.stream().filter(persona -> persona.getId().equals(idEliminar)).findFirst();
		if(p.isPresent()) {
			this.personas.remove(p.get());
		}*/
//		String sql = "delete from persona where id = ?";
//		jdbcTemplate.update(sql, idEliminar);
		repository.deleteById(idEliminar);
	}
	
	@Autowired
	ArticuloService articuloService;
	
	//@Transactional(rollbackFor = ApplicationException.class)
	@Transactional
	public void agregarPar(Articulo articulo, Persona persona) throws ApplicationException {
//		this.agregar(articulo);
//		personaService.agregar(persona);
		this.agregar(persona);
		articuloService.agregar(articulo);
	}
	
	
}
