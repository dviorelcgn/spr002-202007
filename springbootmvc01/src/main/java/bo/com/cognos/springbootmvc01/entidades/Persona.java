package bo.com.cognos.springbootmvc01.entidades;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Persona {
	
	@Id
	private String id;
	private String ci;
	@Column(name = "nombre", length = 70)
	private String nom_bre;
	private String apellido;
	
	@Transient
	private BigDecimal retrasos;
	

}
