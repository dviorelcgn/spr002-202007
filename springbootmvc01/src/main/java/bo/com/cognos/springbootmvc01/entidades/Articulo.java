package bo.com.cognos.springbootmvc01.entidades;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "ALMACEN_ARTICULO")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Articulo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;
	
	@Column(name = "codigo", length = 30, unique = true, nullable = false, updatable = false)
	String codigo;
	
	@Column(name="descripcion", columnDefinition = "TEXT NOT NULL")
	String descripcion;
	
	@Column(name="precio", precision = 10, scale = 2)
	BigDecimal precio;

	@Column(name="ruta_archivo")
	String rutaArchivo;
	
	@Transient
	byte[] archivo;

}
