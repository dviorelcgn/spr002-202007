package bo.com.cognos.springbootmvc01.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bo.com.cognos.springbootmvc01.entidades.Articulo;
import bo.com.cognos.springbootmvc01.service.ArticuloService;

@RestController
@RequestMapping("/rest/libre")
public class LibreRestController {

	@Autowired
	ArticuloService articuloService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/articulocodigo")
	public String obtener(@RequestParam("codigo") String codigo){
		Articulo art = articuloService.obtenerPorCodigo(codigo);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException ignored) {
		}
		return art != null? "Ya existe":"El codigo esta disponible";
	}

	
	
}
