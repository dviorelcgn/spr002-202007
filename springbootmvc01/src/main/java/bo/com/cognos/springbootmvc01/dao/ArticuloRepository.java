package bo.com.cognos.springbootmvc01.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import bo.com.cognos.springbootmvc01.entidades.Articulo;

public interface ArticuloRepository extends JpaRepository<Articulo, Integer>{

	Articulo findByCodigo(String codigo);
	
	
}