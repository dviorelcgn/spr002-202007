package bo.com.cognos.springbootmvc01.entidades.exception;

public class ApplicationException extends RuntimeException {
	public ApplicationException(String mensaje) {
		super(mensaje);
	}
}
