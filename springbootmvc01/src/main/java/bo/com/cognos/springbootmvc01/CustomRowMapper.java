package bo.com.cognos.springbootmvc01;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import bo.com.cognos.springbootmvc01.entidades.Persona;

public class CustomRowMapper implements RowMapper<Persona> {

	@Override
	public Persona mapRow(ResultSet rs, int rowNum) throws SQLException {
		Persona persona = new Persona();
		persona.setId(rs.getString("id"));
		persona.setCi(rs.getString("ci"));
		persona.setNom_bre(rs.getString("nombre"));
		persona.setApellido(rs.getString("apellido"));
		return persona;
	}

}
