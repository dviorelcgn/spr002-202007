package bo.com.cognos.springbootmvc01;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import bo.com.cognos.springbootmvc01.entidades.Articulo;
import bo.com.cognos.springbootmvc01.entidades.Persona;
import bo.com.cognos.springbootmvc01.entidades.exception.ApplicationException;
import bo.com.cognos.springbootmvc01.service.PersonaService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;

@Controller
@RequestMapping("/persona")
public class PersonaController {

//	@Autowired
//	JdbcTemplate jdbcTemplate;
	
	@Autowired
	PersonaService personaService;
	
	@Autowired
	DataSource dataSource;
	
	@RequestMapping(value = "/reportepdf", method = RequestMethod.POST)
	public void reporte(HttpServletResponse response, @ModelAttribute("filtro") String filtro) {
		try {
			InputStream reportStream = getClass().getResourceAsStream("/rpt.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
			Map<String, Object> parametros = new HashMap<>();
			parametros.put("filtro", filtro);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, dataSource.getConnection());
			response.addHeader("Content-Type", "application/pdf");
			response.addHeader("Content-Disposition", "inline; filename=reporte.pdf;");	
			JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
		} catch (JRException | IOException | SQLException e) {
			e.printStackTrace();
		}  
	}
	
	@RequestMapping("/reportehtml")
	public void reportehtml(HttpServletResponse response) {
		try {
			InputStream reportStream = getClass().getResourceAsStream("/rpt.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, dataSource.getConnection());
			JasperExportManager.exportReportToHtmlFile(jasperPrint, "D:/tmp/reporte.html");
			byte[] reporte = Files.readAllBytes(Paths.get("D:/tmp/reporte.html"));
			response.setHeader("Content-Type", "text/html; charset=UTF-8");
			response.setHeader("Content-Disposition", "inline; filename=\"reporte.html\"");
			BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream());
			output.write(reporte);
			output.flush();
		} catch (JRException | SQLException | IOException e) {
		}  
	}
	
	@RequestMapping("/reporteexcel")
	public void reporteenexcel(HttpServletResponse response) {
	try {
		InputStream reportStream = getClass().getResourceAsStream("/rpt.jrxml");
		JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, dataSource.getConnection());
		JRXlsExporter exporter = new JRXlsExporter();
		final ByteArrayOutputStream xlsStream = new ByteArrayOutputStream();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsStream);
		exporter.exportReport();
		byte[] reporte = xlsStream.toByteArray();
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"reporte.xls\"");
		BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream());
		output.write(reporte);
		output.flush();
	} catch (JRException | SQLException | IOException e) {
	}  
	}
	
	@RequestMapping("/reporteword")
	public void reporteword(HttpServletResponse response) {
	try {
		InputStream reportStream = getClass().getResourceAsStream("/rpt.jrxml");
		JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, dataSource.getConnection());
		JRDocxExporter exporterDocx = new JRDocxExporter();
		final ByteArrayOutputStream docStream = new ByteArrayOutputStream();
		exporterDocx.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporterDocx.setParameter(JRExporterParameter.OUTPUT_STREAM, docStream);
		exporterDocx.exportReport();
		byte[] reporte = docStream.toByteArray();
		response.setHeader("Content-Type", "application/msword");
		response.setHeader("Content-Disposition", "attachment; filename=\"reporte.docx\"");
		BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream());
		output.write(reporte);
		output.flush();
	} catch (JRException | SQLException | IOException e) {
		e.printStackTrace();
	}  
	}





	
	@GetMapping("/conjunto")
	public String inicioGuardarConnjunto(Model model) {
		model.addAttribute("persona", new Persona());
		model.addAttribute("articulo", new Articulo());
		return "conjunto";
	}
	
	@PostMapping("/guardarConjunto")
	public String guardarConjunto(Model model, @ModelAttribute("persona")Persona persona,
			@ModelAttribute("articulo")Articulo articulo) {
		try {
			personaService.agregarPar(articulo, persona);
			return "redirect:/persona/conjunto";
		} catch (ApplicationException e) {
			model.addAttribute("mensaje", e.getMessage());
		}
		return "conjunto";
	}
	
	@GetMapping("/")
	public String inicioPersona(Model model) {
		
//		String sql = "INSERT INTO persona (id, ci, nombre, apellido) VALUES (?, ?, ?, ?)";
//		jdbcTemplate.update(sql, UUID.randomUUID().toString(), "00001", "Nombre 1", "Apellido 1");

//		String sql = "SELECT * FROM persona";
//		List<Persona> lista = jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(Persona.class));  
//		for(Persona p: lista) {
//			System.out.println(p);
//		}
		
		
		//List<Persona> personas = PersonaService.getInstance().listar();
		List<Persona> personas = personaService.listar();
		model.addAttribute("personas", personas);
		model.addAttribute("editando", false);
		return "persona";
	}
	
	@GetMapping("/agregar")
	public String preparCrearPersona(Model model) {
		model.addAttribute("persona", new Persona());
		model.addAttribute("editando", true);
		return "persona";
	}
	
	@PostMapping("/guardar")
	public String guardarPersona(Model model, @ModelAttribute("persona")Persona persona, HttpServletRequest request) {
		if(request.getParameter("Cancelar") == null) {
			if(persona.getId() != null && !persona.getId().isEmpty()) {
				//PersonaService.getInstance().editar(persona);
				personaService.editar(persona);
			}
			else {
				//PersonaService.getInstance().agregar(persona);
				personaService.agregar(persona);
			}
		}
		model.addAttribute("editando", false);
		//model.addAttribute("personas", PersonaService.getInstance().listar());
		model.addAttribute("personas", personaService.listar());
		return "redirect:/persona/";
	}
	
	@GetMapping("/editar/{id}")
	public String preparEditarPersona(Model model, @PathVariable String id) {
		//Persona persona = PersonaService.getInstance().obtener(id);
		Persona persona = personaService.obtener(id);
		model.addAttribute("persona", persona);
		model.addAttribute("editando", true);
		return "persona";
	}
	
	@RequestMapping(value = "/borrar", method = RequestMethod.POST)
	public String borrarPersona(Model model, @RequestParam("id")String id) {
		//PersonaService.getInstance().eliminar(id);
		personaService.eliminar(id);
		//model.addAttribute("personas", PersonaService.getInstance().listar());
		model.addAttribute("personas", personaService.listar());
		model.addAttribute("editando", false);
		return "redirect:/persona/";
	}
	
}
