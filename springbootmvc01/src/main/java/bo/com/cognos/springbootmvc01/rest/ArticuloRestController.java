package bo.com.cognos.springbootmvc01.rest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import bo.com.cognos.springbootmvc01.entidades.Articulo;
import bo.com.cognos.springbootmvc01.service.ArticuloService;

@RestController
@RequestMapping("/rest/articulo")
public class ArticuloRestController {

	@Autowired
	ArticuloService articuloService;
	
	@RequestMapping(method = RequestMethod.GET, 
			//produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
			//produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<Articulo> listar(){
		return articuloService.listar();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, 
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
			)
	public Articulo obtener(@PathVariable("id") Integer id){
		return articuloService.obtener(id);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public Articulo crear(@RequestBody Articulo articulo) {
		articuloService.agregar(articulo);
		return articulo;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Articulo editar(@RequestBody Articulo articulo, @PathVariable("id") Integer id) {
		articulo.setId(id);
		articuloService.editar(articulo);
		return articulo;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String eliminar(@PathVariable("id") Integer id) {
		articuloService.eliminar(id);
		return "OK";
	}
	
	private static final String RUTA = "d:/spr002/uploads/";
	
	@RequestMapping(method = RequestMethod.POST, value = "/imagen")
	public Articulo completarImagen(@RequestParam("id") Integer id, @RequestParam("file") MultipartFile file) {	
		Articulo articulo = articuloService.obtener(id);
		String name = UUID.randomUUID().toString() + 
				file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));
		try {
			//articulo.setArchivo(file.getBytes());
			Files.write(Paths.get(RUTA + name), file.getBytes());
		} catch (IOException e) {
		}
		articulo.setRutaArchivo(name);	
		articuloService.editar(articulo);
		return articulo;	
	}
	
	@RequestMapping(value = "/imagen/{id}", method = RequestMethod.GET, 
			produces = {MediaType.IMAGE_JPEG_VALUE})
	public byte[] obtenerImagen(@PathVariable("id") Integer id){
		Articulo art = articuloService.obtener(id);
		byte[] imagen = null;
		try {
			if(art.getRutaArchivo() != null) {
				imagen = Files.readAllBytes(Paths.get(RUTA + art.getRutaArchivo()));	
			}
		} catch (IOException e) {
		} 
		return imagen;
	}


	
	
}
