var stompClient = null;
function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#mensajes").html("");
}
function connect() {
    var socket = new SockJS('/spring-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topicos/mensajes', function (saludo) {
            mostrarSaludo(JSON.parse(saludo.body).mensaje);
        });
    });
}
function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}
function enviarMensaje() {
 stompClient.send("/app/websockets", {}, JSON.stringify({'mensaje': $("#mensaje").val()}));
}
function mostrarSaludo(mensaje) {
    $("#mensajes").append("<tr><td>" + mensaje + "</td></tr>");
}
$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { enviarMensaje(); });
});

